# FORT Technical Exercise
Goal was to refactor `Machine` class into something easier to use and easier add new types of machines to. Code that appeared to be unused, such as *TrimColor* and *IsDark* have been removed. It was not clear how the `Machine` class would have been used elsewhere, it has been assumed it was used as follows:

```
    var machine = new Machine() {
        type = machineTypeId
    };
```

`MachineRefactored` is the refactored version. Both versions are kept side by side to check compatibility in the *TestProject*. The new use for `MachineRefactored` is:

```
     var machine = MachineRefactored.CreateFrom(machineTypeId);
```

New machine types can be added to **MachineTypeSourceGenerator.cs** around lines 24-30. Ideally this would pull from some external source or configuration. 

## FortCodeExercises
* Added **MachineColor.cs** - Simple object for working with machine colors. Future enhancements could include adding *TrimColor* or *IsDark*, both currently appear not to be used.
* Added **MachineRefactored.cs** - Simple object for working with different machine types such as `MachineRefactored.Bulldozer` which is equivalent to `MachineRefactored.CreateFrom(0)`. This object houses the type of machine, name, color, max speed and if the machine has a max speed. Consider removing `hasMaxSpeed` as it is not actually used. 
* Added project reference to *SourceGenerators* which generates concrete classes for known Machine Colors and known Machine Types. Output files are stored in /FortCodeExercises/obj/GeneratedFiles

## SourceGenerators
* Added **MachineColorSourceGenerator.cs** - Generates statically typed colors for white, blue, red, brown, yellow and green.
* Added **MachineTypeSourceGenerator.cs** - Generates statically typed machines such as a brown car with a max speed of 70.
* This seems like way overkill. I don't like the idea of hard coding various machine types, but based off the original **Machine.cs** this seemed to be an okay compromise because machine types can be generated based off of some external data. Ideally we'd discuss the overall domain and come up with a strategy that more closely resembles reality. 

## TestProject
* Added **RefactoringTests.cs** to see if my refactorings broke the original implementation. Tests for each type, 0-4. 