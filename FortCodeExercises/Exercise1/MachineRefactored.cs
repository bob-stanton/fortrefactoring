namespace FortCodeExercises.Exercise1
{
    public partial class MachineRefactored
    {
        private MachineRefactored(int typeId, string name, MachineColor color, bool hasMaxSpeed, int maxSpeed)
        {
            TypeId = typeId;
            Name = name;
            Color = color;
            HasMaxSpeed = hasMaxSpeed;
            MaxSpeed = maxSpeed;
        }
        public int TypeId { get; private set; }
        public string Name { get; private set; }
        public MachineColor Color { get; private set; }
        public bool HasMaxSpeed { get; private set; }
        public int MaxSpeed { get; set; }

        public string Description { get { return $" {this.Color.Name} {this.Name} [{this.MaxSpeed}]."; } }

    }


}

