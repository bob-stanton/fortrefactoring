namespace FortCodeExercises.Exercise1
{
    public partial class MachineColor {

        private MachineColor(string name)
        {
            Name = name;
        }
         public string Name { get; private set; }

        //Possible TODO, add TrimColor and maybe isDark. Currently they do not appear to be used
    }

}   