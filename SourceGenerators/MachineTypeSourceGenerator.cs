﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Text;
using System.Globalization;
using System.Text;

namespace FortCodeExercises.Exercise1
{
    /*
    This is probably way overkill.
    The variable machineTypes generates concrete classes for each type of machine.
    It feels like these machines should be defined in data somewhere, but 
    for the purposes of this exercise based on the information available, this 
    seems like a good compromise. 
    */
    [Generator]
    public class MachineTypeSourceGenerator : ISourceGenerator
    {
        public void Execute(GeneratorExecutionContext context)
        {
            var hasMaxSpeed = true;
            var noMaxSpeed = !hasMaxSpeed;

            //I'd probably want this defined by some external source
            var machineTypes = new[] {
                (0, "bulldozer", "Red", hasMaxSpeed, 80),
                (1, "crane", "Blue", hasMaxSpeed, 75),
                (2, "tractor", "Green", hasMaxSpeed, 90),
                (3, "truck", "Yellow", noMaxSpeed, 70),
                (4, "car", "Brown", noMaxSpeed, 70)
            };

            var textInfo = new CultureInfo("en-US", false).TextInfo;
            var machineTypeSource = new StringBuilder();

            machineTypeSource.Append($@" // Auto-generated code
namespace FortCodeExercises.Exercise1
{{
    public partial class MachineRefactored {{
");
            foreach (var machineType in machineTypes)
            {
                machineTypeSource.Append($@"
            private static MachineRefactored _{machineType.Item2} = new MachineRefactored({machineType.Item1}, ""{machineType.Item2}"", MachineColor.{machineType.Item3}, {machineType.Item4.ToString().ToLower()}, {machineType.Item5});
            public static MachineRefactored {textInfo.ToTitleCase(machineType.Item2)} {{ get {{ return _{machineType.Item2}; }} }}
");
            }

            machineTypeSource.Append($@"

        public static MachineRefactored CreateFrom(int typeId)
        {{
            switch (typeId)
            {{
");

            foreach (var machineType in machineTypes)
            {
                machineTypeSource.Append($@"
                case {machineType.Item1}:
                    return MachineRefactored.{textInfo.ToTitleCase(machineType.Item2)};
");

            }


machineTypeSource.Append($@"
                default:
                    return null;
            }} //switch
        }} //method CreateFrom

    }} //class

}} //namespace
");

            context.AddSource("MachineType.g.cs", SourceText.From(machineTypeSource.ToString(), Encoding.UTF8));

        }

        public void Initialize(GeneratorInitializationContext context)
        {
            // No initialization required for this one
        }
    }
}