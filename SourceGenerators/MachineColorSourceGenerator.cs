﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Text;
using System.Globalization;
using System.Text;

namespace FortCodeExercises.Exercise1
{

    /*
    This is probably way overkill, and I wouldn't do this normally unless 
    this makes sense based on the domain
    */
    [Generator]
    public class MachineColorSourceGenerator : ISourceGenerator
    {
        public void Execute(GeneratorExecutionContext context)
        {
            //I'd probably want this defined by some external source
            var machineColors = new[] {
                "white",
                "blue",
                "red",
                "brown",
                "yellow",
                "green"
            };

            var textInfo = new CultureInfo("en-US", false).TextInfo;
            var source = new StringBuilder();

            source.Append($@" // Auto-generated code
namespace FortCodeExercises.Exercise1
{{
    public partial class MachineColor {{
");
            foreach (var machineColor in machineColors)
            {
                source.Append($@"
            private static MachineColor _{machineColor} = new MachineColor(""{machineColor}"");
            public static MachineColor {textInfo.ToTitleCase(machineColor)} {{ get {{ return _{machineColor}; }} }}
");

            }

            source.Append($@"
    }}
}}
");

            context.AddSource("MachineColor.g.cs", SourceText.From(source.ToString(), Encoding.UTF8));

        }

        public void Initialize(GeneratorInitializationContext context)
        {
            // No initialization required for this one
        }
    }
}