using Xunit;

namespace FortCodeExercises.Exercise1
{
    public class RefactoringTests
    {
        [Fact]
        public void Red_Bulldozer_MaxSpeed80()
        {
            int machineTypeId = 0;
            var machine = new Machine() {
                type = machineTypeId
            };

            var refactored = MachineRefactored.CreateFrom(machineTypeId);

            Assert.Equal(machine.color, refactored.Color.Name);
            Assert.Equal(machine.name, refactored.Name);
            Assert.Equal(machine.description, refactored.Description);
        }

        [Fact]
        public void Blue_Crane_MaxSpeed75()
        {
            int machineTypeId = 1;
            var machine = new Machine() {
                type = machineTypeId
            };

            var refactored = MachineRefactored.CreateFrom(machineTypeId);

            Assert.Equal(machine.color, refactored.Color.Name);
            Assert.Equal(machine.name, refactored.Name);
            Assert.Equal(machine.description, refactored.Description);
        }
        
        [Fact]
        public void Green_Tractor_MaxSpeed90()
        {
            int machineTypeId = 2;
            var machine = new Machine() {
                type = machineTypeId
            };

            var refactored = MachineRefactored.CreateFrom(machineTypeId);

            Assert.Equal(machine.color, refactored.Color.Name);
            Assert.Equal(machine.name, refactored.Name);
            Assert.Equal(machine.description, refactored.Description);
        }

        [Fact]
        public void Yellow_Truck_MaxSpeed70()
        {
            int machineTypeId = 3;
            var machine = new Machine() {
                type = machineTypeId
            };

            var refactored = MachineRefactored.CreateFrom(machineTypeId);

            Assert.Equal(machine.color, refactored.Color.Name);
            Assert.Equal(machine.name, refactored.Name);
            Assert.Equal(machine.description, refactored.Description);
        }

        [Fact]
        public void Brown_Car_MaxSpeed70()
        {
            int machineTypeId = 4;
            var machine = new Machine() {
                type = machineTypeId
            };

            var refactored = MachineRefactored.CreateFrom(machineTypeId);

            Assert.Equal(machine.color, refactored.Color.Name);
            Assert.Equal(machine.name, refactored.Name);
            Assert.Equal(machine.description, refactored.Description);
        }

    }
}